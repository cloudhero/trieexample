﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//implement Node class
    //IsLeaf - Checks if the current node has children
    //FindChildNode - Searches each child of current node for character value.  Returns node if found, null if not found.
    //DeleteChildNode - Searches each child of current node for character value.  Removes from List<Node> with current index in loop.

//implement Trie class
    //Prefix - This is what actually retrieves the list of nodes that represents the provided string
    //Search - This validates what Prefix returns against given string's length and the Prefix's last node value is '$' (end of string)
    //Insert - This creates a new node list for the root node
    //InsertRange (List<string>) - This creates multiple node lists for the root node
    //Delete - This removes nodes one at a time until it reaches a node that has other children nodes

namespace Trie
{
    public class EntryPoint
    {
        static void Main(string[] args)
        {
            Trie myTrie = new Trie();

            myTrie.Insert("Testing");
            
            Console.WriteLine("Testing exists: " + myTrie.Search("Testing"));
            Console.ReadLine();
        }
    }
}
