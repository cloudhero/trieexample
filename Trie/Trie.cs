﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trie
{
    public class Trie
    {
        private readonly Node _root;

        public Trie()
        {
            _root = new Node('^', 0, null);
        }

        //Gets the entire collection of nodes (_root) and searches it one character at a time, then returns the whole collection if it matches the entire string.
        //Returns null if there is no match
        public Node Prefix(string s)
        {
            var currentNode = _root;
            var result = currentNode;

            foreach (var c in s)
            {
                currentNode = currentNode.FindChildNode(c);
                if (currentNode == null)
                    break;
                result = currentNode;
            }

            return result;
        }

        //Prefix method is what actually retreives the Node list that matches the provided string.  Search simply validates it against given string.
        //Obtain node list if it matches the provided string (selects given string from entire collection), otherwise gets null value.  
        //Check if node list Depth is same as the length of string provided *and* that the node list has an ending leaf of value '$'
        public bool Search(string s)
        {
            var prefix = Prefix(s);
            return prefix.Depth == s.Length && prefix.FindChildNode('$') != null;
        }

        public void InsertRange(List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
                Insert(items[i]);
        }

        //Once the existing node is obtained (Prefix), we start at the end, then add a new chain of nodes
        public void Insert(string s)
        {
            var commonPrefix = Prefix(s);
            var current = commonPrefix;

            for (var i = current.Depth; i < s.Length; i++)
            {
                var newNode = new Node(s[i], current.Depth + 1, current);
                current.Children.Add(newNode);
                current = newNode;
            }

            current.Children.Add(new Node('$', current.Depth + 1, current));
        }

        //Given the  string, validate it exists in the current list
        //If so, get node list from entire collection
        //With node list of provided string, 
        public void Delete(string s)
        {
            if (Search(s))
            {
                //Gets the leaf (end) node
                var node = Prefix(s).FindChildNode('$');

                //Until we reach a node with other children than the current one, delete the child node from the current node's parent (so delete the current node)
                while (node.IsLeaf())
                {
                    var parent = node.Parent;
                    parent.DeleteChildNode(node.Value);
                    node = parent;
                }
            }
        }
    }
}
